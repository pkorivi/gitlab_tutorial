#include "math_app/math_app.h"

#include <iostream>

int main()
{
    detail::BaseMathFuctions base_math_functions;

    std::cerr << "Operation count : " << base_math_functions.GetOperationCounter() << std::endl;
    std::cerr << "Addition 14+15 =  " << base_math_functions.PerformAddition(14, 15) << std::endl;
    std::cerr << "Operation count : " << base_math_functions.GetOperationCounter() << std::endl;

    return 0;
}
