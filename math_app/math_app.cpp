#include "math_app.h"

namespace detail
{

BaseMathFuctions::BaseMathFuctions() : operation_counter_{0} {}

int BaseMathFuctions::GetOperationCounter() const
{
    return operation_counter_;
}

int BaseMathFuctions::PerformAddition(const int a, const int b)
{
    operation_counter_++;
    return a + b;
}

int BaseMathFuctions::CalculateFactorial(const int value)
{
    if (value < 0)
    {
        return 0;
    }
    else if (value == 0 || value == 1)
    {
        return 1;
    }

    return value * CalculateFactorial(value - 1);
}

}  // namespace detail
