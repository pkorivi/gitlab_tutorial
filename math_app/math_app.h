//
// @
//

#ifndef MATH_APP_MATH_APP_H
#define MATH_APP_MATH_APP_H

namespace detail
{
class BaseMathFuctions
{
  public:
    BaseMathFuctions();
    int GetOperationCounter() const;
    int PerformAddition(const int a, const int b);
    int CalculateFactorial(const int a);

  private:
    int operation_counter_;
};
}  // namespace detail

#endif  // MATH_APP_MATH_APP_H
