#include "math_app/math_app.h"

#include <gtest/gtest.h>

#include <iostream>
#include <tuple>

namespace
{

// Simple tests
TEST(Init, Given_Initialization_Expect_InitValues)
{
    // Given
    detail::BaseMathFuctions base_math_funcions_{};
    // Expect two strings not to be equal.
    EXPECT_EQ(base_math_funcions_.GetOperationCounter(), 0);
    // Expect equality.
    EXPECT_EQ(7 * 6, 42);
}

// tests with fixture
class BaseMathFuctionsFixture : public ::testing::Test
{
  public:
    BaseMathFuctionsFixture() : base_math_funcions_{} {}

    int PerformAddition(const int a, const int b) { return base_math_funcions_.PerformAddition(a, b); }
    int GetOperationCounter() const { return base_math_funcions_.GetOperationCounter(); }
    int CalculateFactorial(const int value) { return base_math_funcions_.CalculateFactorial(value); }

  protected:
    void SetUp() override {}

    void TearDown() override {}

  private:
    detail::BaseMathFuctions base_math_funcions_{};
};

TEST_F(BaseMathFuctionsFixture, GivenData_WhenAddition_ExpectRightAnswer)
{
    std::cerr << "Execute Test Case" << std::endl;
    const auto result = PerformAddition(2, 4);
    EXPECT_EQ(result, 6);
    EXPECT_EQ(GetOperationCounter(), 1);
}

// test with params
class BaseMathFuctionsFixture_WithParam : public ::testing::TestWithParam<std::tuple<int, int, int>>
{
  public:
    BaseMathFuctionsFixture_WithParam() : base_math_funcions_{} {}

    int PerformAddition(const int a, const int b) { return base_math_funcions_.PerformAddition(a, b); }
    int GetOperationCounter() const { return base_math_funcions_.GetOperationCounter(); }

  private:
    detail::BaseMathFuctions base_math_funcions_{};
};

INSTANTIATE_TEST_SUITE_P(MathFunctionParametrizedTest,
                         BaseMathFuctionsFixture_WithParam,
                         ::testing::Values(std::make_tuple(1, 2, 3),
                                           std::make_tuple(1, 3, 4),
                                           std::make_tuple(9999991, 555555, 10555546),
                                           std::make_tuple(-500, -800, -1300)));

TEST_P(BaseMathFuctionsFixture_WithParam, GivenData_WhenAddition_ExpectRightAnswer)
{
    const auto param = GetParam();
    const auto result = PerformAddition(std::get<0>(param), std::get<1>(param));
    EXPECT_EQ(result, std::get<2>(param));
    EXPECT_EQ(GetOperationCounter(), 1);
}

class CalculateFactorialFixture_WithParam : public BaseMathFuctionsFixture,
                                            public ::testing::WithParamInterface<std::pair<int, int>>
{
};

INSTANTIATE_TEST_SUITE_P(CalculateFactorialParametrizedTest,
                         CalculateFactorialFixture_WithParam,
                         ::testing::Values(std::make_pair(1, 1),
                                           std::make_pair(5, 120),
                                           std::make_pair(25, 2076180480),
                                           std::make_pair(-2, 0)));

TEST_P(CalculateFactorialFixture_WithParam, GivenData_WhenFactorialIsCalculated_ExpectCorrectResult)
{
    const auto param = GetParam();
    const auto result = CalculateFactorial(std::get<0>(param));
    EXPECT_EQ(result, std::get<1>(param));
}

}  // namespace
